# Do *NOT* build this file in CI/CD, it's pulls down a ton of data.
ARG VARIANT="ubuntu-20.04"
FROM ubuntu:20.04 as build

# Download depenencies.
ENV DEBIAN_FRONTEND=noninteractive
RUN apt update && apt install -y  build-essential clang bison flex libreadline-dev \
    gawk tcl-dev libffi-dev git mercurial graphviz libeigen3-dev \
    xdot pkg-config python python3 libftdi-dev \
    qt5-default python3-dev libboost-all-dev cmake gtkwave git automake

# Download source code.
RUN git clone https://github.com/YosysHQ/nextpnr nextpnr && git clone https://github.com/cliffordwolf/icestorm.git icestorm

# Build code from sources.
# Make a that is rooted at /install, so that we can copy it easily.
RUN ( cd icestorm; sed -i "s/PREFIX ?= \/usr\/local/PREFIX ?= \/install/g" config.mk; \
    make -j$(nproc); make install)
RUN ( cd nextpnr; git submodule update --init --recursive; cmake -DARCH=ice40 -DBUILD_GUI=OFF -DCMAKE_INSTALL_PREFIX="/install"; make -j$(nproc); make install )
RUN ls

# Try and reduce size of image by cutting out development libraries.
FROM mcr.microsoft.com/vscode/devcontainers/cpp:0-${VARIANT} as deploy
ENV DEBIAN_FRONTEND=noninteractive
RUN apt update && apt install -y iverilog yosys git make \
    gawk libffi-dev python3 libftdi-dev g++\
    python3-dev gtkwave verilator
COPY --from=build /install /usr/local
