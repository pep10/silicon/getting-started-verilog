# Tutorial 1: your first verilog component.
This tutorial will show you a basic module in verilog.
Google will be your friend when you want to create your own one.
This tutorial will assumes that you will use my provided docker image, but if you followed the instructions from tutorial 0, you are free to use your host machine too.

## Running the Docker script
Switch to this directory (the one where this README is located).
Ensure that Docker is running on your machine, and execute `sudo docker build -t tutorial-01 .`.

This will copy our source code over (located in ./src/) to the docker container.
The verilog definitions (`top.v`) will then be run through the iverilog compiler.
Lastly, the testbench (`tb.v`) will be executed, and will display a bit toggling every 5 timesteps.

## Visualizing
Please ensure you have installed `gtkwave` via `sudo apt install gtkwave`.
So, our docker container has a `.vcd` file in it, which contains a data needed to visualize the transition waveforms.
Copying a file from a docker container can be a pain, so do the following.

```
sudo docker build -t tutorial-01 .
container_id=$(sudo docker run -d tutorial-01 sleep inf)
sudo docker cp $container_id:/work/src/test.vcd .
sudo docker rm --force $container_id
```

Open up the copied vcd file with gtkwave using:
```
gtkwave test.vcd
```

### See the Results
- gtkwave should open with a time graph.
- On the left side there is a list of Type | Signals.
- Select the reg | state
- You should see the on off wave patter show up on the time graph

## Congratz! You have made it through tutorial 01. Please procced to 02.

