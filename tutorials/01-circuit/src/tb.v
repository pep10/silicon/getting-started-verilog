`include "./top.v"

// Modules that provides tests for actual source code are called "testbenches."
// It is imperative that we throughly test our designs, our we might ship a broken chip. 
module testbench();
	// Inputs to our blinking module.
	reg clock;
	wire value;

	// Create module-under-test
	top mytop(clock, value);
	
	// Dump the state of values w.r.t. to a wave file, which can be visualized.
	// "initial" blocks execute at the 0'th timestep of the simulation.
	initial begin
		$dumpfile("test.vcd");
		$dumpvars(0, testbench);
	end

	// Initialize inputs, and set up code that "watches" our modules outputs.
	initial begin
		// All values in verilog are in terms of bits.
		// You must specify how many bits (0), and then a number,
		// like b0, d8, or hf for binary 0, decimal 8, and hex "f" respectively.
		// This sizing also applies to things like registers and wires.
		clock = 1'b0;
		// A verilog magic function that prints to the console.
		$display("Time \t  LED");
		// $monitor basically does a $display any time a listed value changes.
		// Only works in iverilog / vvp.
		$monitor ("%g\t   %b" , $time, value);
	end

	// Always blocks execute over and over and over again.
	always begin
		// Tells the simulator to pause for 5 ticks.
		#5;
		// And then toggle the clocj
		clock <= ~clock;
	end

	// A separate loop that waits for 100 time units, then kills the simulation.
	always begin
        #100;
		$finish;
    end
endmodule