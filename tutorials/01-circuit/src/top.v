module top(
	// Declare what values we want visible to the outside world.
	// By default, values are assumed to be 1 bit.
    input CLOCK,
    output  led
);

    /* Registers are values that per */
    reg state = 1'b0;
    
    /* Force led to always have the same value as state.
	   This is likely implemented using a combinational circuit.
	*/
    assign led = state;
    
    /* Executes every time CLOCK changes */
    always @ (posedge CLOCK) begin
		// Invert our state once per clock cycle
        state <= ~state;
    end
endmodule