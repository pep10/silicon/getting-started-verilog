# Tutorial 2: Reading and understanding Verilog code
This tutorial will show you a functioning program in Verilog.
You will be able to see how ports are accessed and writen to. You will also get a good example of how each file is connected and how to begin programming in verilog.

## What each File is Doing
To start lets just see what each files function is.

### 1) dff.v
This is the function of the program.  The program itself is a ......(Finish)
### 2) top.v
This is our main function. It sets inputs and outputs for the circut. It also calls the function in dff and organizes the results.
### 3) tb.v
This is the Test Bench which will call our main function in top and test our dff program. Then it will output the results of our program in a waveform pattern, so that we can test it is working.

## Going Deeper to the programming
Now we will look at the verilog code and walk through what is happening

### 1) DFF

### 2) top

### 3) Test Bench

## Some Format styles and Tricks to use.




#Ignore below just using for example of git writing.

Ensure that Docker is running on your machine, and execute `sudo docker build -t tutorial-01 .`.

## Visualizing
Please ensure you have installed `gtkwave` via `sudo apt install gtkwave`.
So, our docker container has a `.vcd` file in it, which contains a data needed to visualize the transition waveforms.
Copying a file from a docker container can be a pain, so do the following.

```
sudo docker build -t tutorial-01 .
container_id=$(sudo docker run -d tutorial-01 sleep inf)
sudo docker cp $container_id:/work/src/test.vcd .
sudo docker rm --force $container_id
```

Open up the copied vcd file with gtkwave using:
```
gtkwave test.vcd
```

### See the Results
- gtkwave should open with a time graph.
- On the left side there is a list of Type | Signals.
- Select the reg | state
- You should see the on off wave patter show up on the time graph

## Congratz! You have made it through tutorial 01. Please procced to 02.
