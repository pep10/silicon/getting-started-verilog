module DFF_Sync_Reset (
    input wire[WIDTH-1:0] D,
    input wire clock,
    input wire sync_reset,
    // We want Q to mantain its value between cycles, so we declare it as a register.
    // Register do not always translate to hardware registers, but it declares
    // your intent to save state using some kind of storage element.
    // See:
    //      https://www.chipverify.com/verilog/verilog-data-types
    output reg[WIDTH-1:0] Q);
    parameter WIDTH = 1;

    // D-flip flops (DFF) perform their work at one edge of the clock cycle.
    // I arbitrarily chose the rising (positive) edge.
    always @(posedge clock) begin
        // A DFF with syncrhonous reset allows you to force the output to 0 when
        // clocked, even if D has some other data on it.
        if(sync_reset == 1'b1)
            Q[WIDTH-1:0] <= {WIDTH+1{1'b0}};
        // Otherwise, pass the information from D through to Q, and persist Q.
        else 
            Q <= D; 
    end 
endmodule 
