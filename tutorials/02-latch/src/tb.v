`include "./top.v"

module testbench();
	//https://www.chipverify.com/verilog/verilog-in-a-nutshell
	// Declare a verilog parameter named `WIDTH.
	// Parameters are like compile time constants we can use in place on raw integers.
	// They are imperative for making a single circuit work on bit patterns of different lengths.
	// See:
	//		https://www.chipverify.com/verilog/verilog-parameters
	specify
		specparam WIDTH=2;
	endspecify

	// A [#:#] after  the keywords reg or wire.
	// Otherwise, the value is assumed to be a single bit.
	reg clock;
	reg [WIDTH-1:0] data;
	wire [WIDTH-1:0] value;

	// Create module-under-test
	// Set the compile time constant `WIDTH` of mytop to equal our `WIDTH`.
	// the .WIDTH refers to the name of the parameter in top, while the WIDTH
	// in () refers to our copy.
	// For more on Verilog parameters, see:
	//		https://www.chipverify.com/verilog/verilog-parameters
	// Then bind wires / registers as normal 
	top #(.WIDTH(WIDTH)) mytop(clock, data, value);
	

	initial begin
		$dumpfile("tutorial-02.vcd");
		$dumpvars(0, testbench);
	end

	initial begin
		// Reach inside the DFF, and force-set it to 0.
		// This general technique will not apply in verilator,
		// but it is a (cheaty) way to initialize modules.
		// The {value{some constant}} notation is shorthand for saying
		// "Concatenate `value` copies of `some constant` together and assign it".
		mytop.dff.Q={WIDTH{1'b0}};
		// Clocks are always 1 bit.
		clock = 1'b0;
		// Un-prefixed constants size can (sometimes) be inferred.
		// Trial-and-error is usually the best way to determine if you need a width prefix.
		data = 0;
		// We don't need to initialize `value`, since it is set (or `driven`) bymtop.dff

		$display("Time \t  LED");
		$monitor ("%g\t   %b" , $time, value);
	end

	// To show that toggling data does nothing without asserting the clock,
	// we will toggle the data more often than we toggle the clock.
	// For information on ALWAYS blocks, see:
	//		https://www.chipverify.com/verilog/verilog-always-block
	always begin 
		#3;
		clock <= ~clock;
	end

	always begin
		#2;
		// Bitwise operations apply to all bits in an input.
		// For more on operations, see:
		// 		https://www.chipverify.com/verilog/verilog-operators
		data <= ~data;
	end

	// A separate loop that waits for 100 time units, then kills the simulation.
	always begin
        #100;
		$finish;
    end
endmodule