// Make modules from dff.v (like DFF_Sync_Reset) available in this source file.
`include "./dff.v"

// Create a module that is variable width.
// This module is just a friendly wrapper around out
// N-bit D flip-flop.
module top #(parameter WIDTH = 1) (
    input wire CLOCK,
	// Notice that sizes go *before* the variable name.
	// This below notation describes `WIDTH`-bit values.
	// Alternatively, input wire D [WIDTH-1:0] would be declaring an array
	// with `WIDTH` entries. Arrays are not trivial to "hook together", whereas
	// individual wires and registers are.
	// For more details, see:
	//		https://www.chipverify.com/verilog/verilog-arrays
	input wire [WIDTH-1:0] D,
    output wire [WIDTH-1:0] led
);
	// Set the WIDTH parameter of the DFF to be equal to our WIDTH
	DFF_Sync_Reset dff(D, CLOCK, 1'b0,led);
	defparam dff.WIDTH = {WIDTH};
	// The above notation is an alternative to that seen in top.v
	// In the present case, the equivalent notation would be:
	//DFF_Sync_Reset #(.WIDTH(WIDTH)) dff(D, CLOCK, 1'b0,led);

endmodule