# Learning to Build Combinational Circuits with a Full Adder.

In this tutorial, we will look at 2 implementations of a (full adder)[https://theorycircuit.com/wp-content/uploads/2018/07/full-adder-circuit.png].

These two examples will juxtapose some "good" and "bad" elements of verilog design.
The lessons to be taken from these examples are summarized below:
* Use parameters to define the sizes of elements, unless you are 110% sure something will never change in size.
  For example, a (Universal Asynchronous Receiver-Transmitter (UART))[https://en.wikipedia.org/wiki/Universal_asynchronous_receiver-transmitter] will be limited in bit size, whereas a register bank shouldn't be.
* Describe circuit behavior, not circuit implementation. 
  Tell verilog what you want done (add two sets of numbers, multiplex something) rather than design the logic gates for how to do it.
  Verilog will generate smaller and faster circuits than you.
* Don't use magic constants. Using `#13`in a testing loop *will* make you sad in the future.
* Use modules liberally.
  Don't try and cram all the behavior into one class, otherwise you won't benefit from reuse like we'll see in tutorial 04.
* There is no built in `assert` statement. 
  We will revisit assertions in tutorials 05 and 06.