// If you end up with modules that look like this, something is going wrong.
// Talk to me and I'll help you work through it.
// While breaking out behavior into other modules is a good idea, it can have
// terrible design consequences (see lines 38-41).
module adder(input A, input B, input C_in, output C, output c_out);

	wire a_xor_b, chained;
	// Wait, why are you trying to implement the logic gates behind addition?
	// If verilog has an operator for it, use it!
	// When using verilog, you should be describing how the chip *behaves* rather
	// than how to do it. Some chips may include magic adder circuits, but
	// by implementing it myself, I prevent the verilog compiler from taking
	// advantage of those features.
	// Sometimes logic operations are necessary (see next example),
	// but a module that looks like this is suspicoius.
	assign a_xor_b = A ^ B;
	assign chained = a_xor_b ^ C_in;
	assign c_out = A & B | C_in & a_xor_b;
	assign C = chained;
endmodule

// Instead of making this module paramaterized, it's got an ugly suffix for length.
// Now, it may not always be possible to make any circuit any size (like a memory
// subsytem), but you should be cautious about creating fixed-size modules
// when you *know* that you will use it again (like an adder-with-carry);
module FullAdder4Bit (
	// Yikes, magic constants for upper bounds!
    input [3:0] A,
    input [3:0] B,
    output wire[3:0] out,
	output wire carry
);

	// Because my adder definition is bad, I can't take advantage of
	// vectorized computation. So I must manually bind all wires.
	wire carries[4:0];
	assign carry = carries[4];
	adder b0(A[0], B[0], 1'b0      , out[0], carries[1]);
	adder b1(A[1], B[1], carries[1], out[1], carries[2]);
	adder b2(A[2], B[2], carries[2], out[2], carries[3]);
	adder b3(A[3], B[3], carries[3], out[3], carries[4]);

endmodule 

