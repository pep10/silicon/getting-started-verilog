// Declare a circuit that takes in two binary inputs and outputs the sum.
module FullAdder #(parameter WIDTH = 1) (
    input [WIDTH-1:0] A,
    input [WIDTH-1:0] B,
    output wire[WIDTH-1:0] out,
	output wire carry
);

	// Perform addition with N+1 bits. Then, getting the carry-out only requires
	// fetching the most significant bit.
	wire [WIDTH:0] value;
	// Addition implemented using verilog operator! Easy to read and understand
	assign value = A + B;
	// Grab the low order N bits which compose the sum.
	assign out = value[WIDTH-1:0];
	// High order / most significant bit is the N+1'th bit, and is thus a carry out.
	assign carry = value[WIDTH];

endmodule 

