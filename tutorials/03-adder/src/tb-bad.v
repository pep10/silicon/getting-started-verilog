`include "./bad-adder.v"
module testbench();
	// Declare the inputs & outputs to hook up to the adder.
	// Yikes, magic constants for upper-bounds are not good!
	reg [3:0] A;
    reg [3:0] B;
	wire [3:0] out;
    wire carry_out;

	// Construct the adder.
	// Suspicous suffix on name.
	FullAdder4Bit adder(A, B, out, carry_out);

	initial begin
		$dumpfile("tb-bad.vcd");
		$dumpvars(0, testbench);
	end

	initial begin
		// Initialize our adder's inputs to 0.
		A = 0;
		B = 0;

		$display("Time \t     A     B   Cout+C");

		// Log any changes to inputs or outputs.
		// Since our circuit is entirely combinational, inputs/outputs update on same timestep.
		$monitor ("%g\t   %b  %b  %b%b" , $time, A, B, carry_out, out);
	end

	// Increment A every timestep.
	always begin 
		#1;
		A = A + 1;
	end

	// Increment B after A has "wrapped around".
	always begin
		// More magic constants that depend on module width.
		#16;
		B = B + 1;
	end

	// A separate loop that waits for 255 time units, then kills the simulation.
	always begin
		// Yet another constant that depends on module width.
        #255;
		$finish;
    end
endmodule