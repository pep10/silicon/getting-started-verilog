// Declare a circuit that takes in two binary inputs and outputs the sum.
module AdderSubtracter #(parameter WIDTH = 1) (
    input [WIDTH-1:0] A,
    input [WIDTH-1:0] B,
	input carry_in,
	input invert_b,
    output wire[WIDTH-1:0] out,
	output wire carry_out
);
	FullAdder #(.WIDTH(WIDTH)) adder(A, B ^ {WIDTH{invert_b}}, carry_in, out, carry_out);

endmodule 

