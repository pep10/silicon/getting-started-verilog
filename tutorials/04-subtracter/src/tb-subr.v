`include "./adder.v"
`include "./subr.v"
module testbench();
	/* List all inputs / outputs / parameters.*/
	// Our testbench is self-contained, so it only has parameters/
	// Specify how many bits we want in our adder.
	specify
		specparam WIDTH = 4;
	endspecify


	/* Declare all signals.*/
	// Declare "local variables" to hook up to the adder.
	// Good style to refer to `WIDTH` rather than a magic constant.
	reg [WIDTH-1:0] A;
    reg [WIDTH-1:0] B;
	reg carry_in;
	reg invert;
	wire [WIDTH-1:0] out;
    wire carry_out;

	/* Instantiate modules.*/
	// Construct the adder.
	// Notice that I'm passing the width of A, B to the adder
	// via the .WIDTH parameter.
	AdderSubtracter #(.WIDTH(WIDTH)) adder(A, B, carry_in, invert, out, carry_out);


	/* Module behavior or unit tests.*/
	initial begin
		$dumpfile("subr.vcd");
		$dumpvars(0, testbench);
	end

	initial begin
		// Initialize our adder's inputs to 0.
		A = 0;
		B = 0;
		carry_in = 1;
		invert = 1;

		$display("Time \t     A     B   Cout+C");

		// Log any changes to inputs or outputs.
		// Since our circuit is entirely combinational, inputs/outputs update on same timestep.
		$monitor ("%g\t   %b  %b  %b%b" , $time, A, B, carry_out, out);
	end

	// Increment A every timestep.
	always begin 
		// Even though loop bound is not a function of module width,
		// the #1 is self-explanatory (i.e. run every timestep)
		#1;
		A = A + 1;
	end

	// Increment B after A has "wrapped around".
	always begin
		// Good, loop bound if a function of module width.
		#((2<<WIDTH-1));
		B = B + 1;
	end

	// A separate loop that waits for 255 time units, then kills the simulation.
	always begin
		// Good, loop bound is a function of module width.
        #((2<<(2*WIDTH-1))-1);
		//words
		$finish;
    end
endmodule