`include "./adder.v"
`include "./subr.v"

// Declare a circuit that takes in two binary inputs and outputs the sum.
module top #(parameter WIDTH = 4) (
    input [WIDTH-1:0] A,
    input [WIDTH-1:0] B,
	input carry_in,
	input invert_b,
    output wire[WIDTH-1:0] out,
	output wire carry_out
);
	AdderSubtracter #(.WIDTH(WIDTH)) adder(A, B, carry_in, invert_b, out, carry_out);

endmodule 

