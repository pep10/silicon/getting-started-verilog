#include <stdlib.h>
#include "Vtop.h"
#include "verilated.h"


#include <vector>
#include <iostream>

int main(int argc, char **argv) {
	// Initialize Verilators variables
	Verilated::commandArgs(argc, argv);
	// Create an instance of our module under test
	Vtop *tb = new Vtop;
    static const int NUM_BITS = 4;
    uint64_t fails = 0;
    uint64_t runs = 0;
    for(int invert = 0; invert <2; invert++) {
        for(int carry_in=0; carry_in<2;carry_in++){
            for(int a=0; a<(1<<NUM_BITS);a++) {
                for (int b=0; b<(1<<NUM_BITS); b++) {
                    tb->A = a;
                    tb->B = b;
                    tb->carry_in = carry_in;
                    tb->invert_b = invert;
                    tb->eval();
                    runs++;
                    // Mask out any higher order bitd from sign extension.
                    auto tx_b = (invert ? ~b : b) & (1<<NUM_BITS)-1;
                    if(tb->out != (a+tx_b+carry_in) % (1<<NUM_BITS)) {
                        fails++;
                        std::cout <<a<<" "<< b<<" "<<" "<<carry_in<<" "<<invert<<" "<<(int)tb->out << std::endl;
                        std:: cout << tx_b << " " << (a+tx_b+carry_in) % 16 << std::endl;
                    }
                    else if((bool)tb->carry_out != (bool)(a+tx_b+carry_in)&(1<<(NUM_BITS+1))) {
                        fails++;
                    }
                
                }
            }
        }
    }
    std::cout << "Ran " << runs << " unit tests." << std::endl;
    if(fails == 0) {
        std::cout << "Passed all unit tests." << std::endl;
    }
    else if(fails == 1) {
        std::cout << "Failed 1 unit test." << std::endl;
    }
    else{
        std::cout << "Failed " << fails <<" unit tests." << std::endl;
    }
    if(fails == 0) exit(EXIT_SUCCESS);
    else exit(EXIT_FAILURE);
}
